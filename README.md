# 00 Pilot Snake

00 Pilot is designed to provide an ORM like experience for python users of the [00-api](https://gitlab.com/frame-00/00-api).  Pilot snake leverages the root api url and OPTIONs calls to automatically generate a client so you do not need to deploy need code whenever you're schemas change.  00 lazy loads all resources so that they happen at the last possible moment and caches them in many cases to avoid reloading them.

## Getting Started

Supports Python 3.x

```
pip install https://gitlab.com/frame-00/00-pilot-snake.git#egg=00-pilot
```

Since Pilot Snake is made as a generalized client you will likely want to create file or package for use/reuse.  Here is an example of how you might do this

```
import os

from zerozero_pilot import build_client

# TODO: update to correct urls
API_URL = os.environ.get("<name of api>_API_URL", "prod_url")
API_TOKEN = os.environ["<name of api>_API_TOKEN"]

build_client(API_URL, API_TOKEN, __name__)
```


## Relationship to API

If you were to build a client for an API with these models in the test_app app then the client module/package will have a test_app module and inside it there will be an Example and ExamplesChild class.

```
from django.db import models

from zerozero.registry import register


class Example(models.Model):
    char = models.CharField(max_length=10)


register(Example, {"exclude": ["excluded_field"]})


class ExamplesChild(models.Model):
    parent = models.ForeignKey(
        Example,
        on_delete=models.CASCADE,
        related_name="children",
        related_query_name="children",
    )


register(ExamplesChild)
```


```
from ... import client

Example = client.test_app.Example
ExamplesChild = client.test_app.ExamplesChild

```


Example and ExamplesChild each represent a single model and all for the creation of Example and ExamplesChild objects

### Model

```
from client.test_app import Example
example = Example.create(char="test")
examples = Example.list()
example = Example.get(pk=123)
examples_after_123 = Examples.list(where={ "pk__gte": 123 })
```

### Model Instance 
Example.list, create, and get both return model instances. Each model instance has properties for each field and update/save methods

```
from client.test_app import Example

examples = Example.list()
example = examples[0]
print(example.char)
example.char = 'test'
example.save()
example.update(char='test 2')

```

#### Model Instance with related model

```
from client.test_app import Example, ExamplesChild

children = ExamplesChild.list(where={"parent__char": "test" })
child = children[0]
print(child.parent.char)
example = Example.get(pk=1)
example.parent = example
example.save()
```

#### Extra Info

Models have a metadata property wit the results of the OPTIONs call for each resource.  This should document the schema in a quick way from the command line.



## Contributing

### Getting Started

00 Pilot should work on python 3 and is currently tested for 3.8 and 3.9.  If anyone wants to update our tests to pass for 3.X please doso and we will update tox.

```
conda env create -f conda_env.yml
conda activate zerozero_pilot
pip install -e ."[dev]"
```

### Testing

```
pytest
```

We use the pytest-randomly, cov, and repeat libraries and you can discover helpful options here

```
pytest --help
```

We use tox with conda to test with different python versions

```
tox
```
