import os
import pytest
import sys
from unittest import mock


from zerozero_pilot import exceptions
from zerozero_pilot import get_model, get_app
from zerozero_pilot import build_client as _build_client


@pytest.fixture
def rebuild_client(
    expected_example_list_url,
    expected_exampleschild_list_url,
    expected_root_url,
):
    """This recreates the module for each current running test
    this is different than build_client as build_client only
    creates a Root class the first time.
    Use this fixture for tests testing build_client directly
    """
    TOKEN = "test-token"
    example_path = "test_app.Example"
    child_path = "test_app.ExamplesChild"
    mock_response = pytest.mock_response_json(
        {
            example_path: expected_example_list_url,
            child_path: expected_exampleschild_list_url,
        },
        200,
    )
    new_module_name = (
        os.environ.get("PYTEST_CURRENT_TEST").split(":")[-1].split(" ")[0]
    )
    new_module = type("", (), {})()
    sys.modules[new_module_name] = new_module
    with mock.patch("zerozero_pilot.request.authenticated_request") as ar:
        ar.return_value = mock_response
        _build_client(expected_root_url, TOKEN, new_module_name)
        ar.assert_called_with("GET", expected_root_url)
    return new_module, new_module_name


def test_build_client(
    expected_exampleschild_list_url,
    expected_example_list_url,
    rebuild_client,
):
    new_module, new_module_name = rebuild_client
    assert (
        expected_exampleschild_list_url
        == new_module.test_app.ExamplesChild.list_url
    )

    assert expected_example_list_url == new_module.test_app.Example.list_url
    assert "test_app.Example" == str(new_module.test_app.Example)


def test_get_model(
    rebuild_client,
    expected_example_list_url,
):
    new_module, new_module_name = rebuild_client
    Example = get_model("test_app.Example", new_module)

    assert expected_example_list_url == Example.list_url


def test_get_app(
    rebuild_client,
    expected_example_list_url,
):
    new_module, new_module_name = rebuild_client
    test_app = get_app("test_app", new_module)

    assert expected_example_list_url == test_app.Example.list_url


def test_build_client_non_200(
    expected_example_list_url,
    expected_exampleschild_list_url,
    expected_root_url,
):
    TOKEN = "test-token"

    mock_response = pytest.mock_response_json(
        {
            "test_app.Example": expected_example_list_url,
            "test_app.ExamplesChild": expected_exampleschild_list_url,
        },
        400,
    )

    with mock.patch("zerozero_pilot.request.authenticated_request") as ar:
        ar.return_value = mock_response
        with pytest.raises(exceptions.ZeroZeroAPIException) as excinfo:
            _build_client(expected_root_url, TOKEN, __name__)
        assert ar.return_value.status_code != 200
