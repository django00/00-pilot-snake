import sys
import pytest

from unittest import mock
from zerozero_pilot import request
from zerozero_pilot import exceptions


def test_authenticated_request(set_token):
    with mock.patch("zerozero_pilot.request.request") as r:
        args = ["arg1", "arg2"]
        kwargs = {"kw1": "foo", "kw2": "bar"}
        expected_kwargs = kwargs.copy()
        expected_kwargs["headers"] = {
            "Authorization": "Token test-token",
            "Content-Type": "application/json",
        }
        request.authenticated_request(*args, **kwargs)
        r.assert_called_with(*args, **expected_kwargs)


def test_authenticated_request_no_token():
    request.TOKEN = None
    with pytest.raises(exceptions.ZeroZeroClientException) as excinfo:
        request.authenticated_request()
        assert "No token exception" == excinfo
