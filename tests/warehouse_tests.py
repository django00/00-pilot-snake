import pytest
import json
from datetime import datetime
from pandas import DataFrame

from unittest import mock
from tests.conftest import mock_response_json
from requests.models import Response

from zerozero_pilot.model import Model
from zerozero_pilot.warehouse import (
    run_query_report_task,
    get_ready_query_reports,
)

MOCKDATE = datetime(2000, 1, 1, 0, 0, 0)


@pytest.fixture()
def mock_datetime(monkeypatch):
    class mockdatetime:
        @classmethod
        def now(cls, tz):
            return MOCKDATE

    monkeypatch.setattr("zerozero_pilot.warehouse.datetime", mockdatetime)


def test_query_report_task(
    build_client,
    mock_datetime,
    expected_example_list_url,
    expected_query_report_options,
    expected_query_report_log_options,
    expected_query_report_detail_url,
    expected_query_report_log_detail_url,
    expected_query_report_detail_data,
    expected_query_report_log_start_data,
):
    engine_mock = mock.Mock()

    expected_report_log_params = {
        "url": expected_query_report_log_detail_url,
        "id": 1,
        "name": "rep1",
        "report": expected_query_report_detail_url,
        "start_time": MOCKDATE.isoformat(),
        "end_time": MOCKDATE.isoformat(),
        "success": True,
        "error": None,
    }

    responses = [
        pytest.mock_response_json(
            expected_query_report_options,
            200,
        ),
        pytest.mock_response_json(
            expected_query_report_detail_data,
            200,
        ),
        pytest.mock_response_json(
            expected_query_report_log_options,
            200,
        ),
        pytest.mock_response_json(
            expected_query_report_log_start_data,
            201,
        ),
        pytest.mock_response_csv(
            [{"data": "some data"}],
            200,
        ),
        pytest.mock_response_json(
            expected_report_log_params,
            200,
        ),
    ]

    with mock.patch(
        "zerozero_pilot.model.authenticated_request"
    ) as request_mock:
        request_mock.side_effect = responses
        with mock.patch(
            "zerozero_pilot.warehouse.create_engine", return_value=engine_mock
        ):
            with mock.patch.object(DataFrame, "to_sql") as sql_mock:
                run_query_report_task(1, "warehouse_url", build_client)

                sql_mock.assert_called_with(
                    "rep1", engine_mock, if_exists="replace"
                )

                request_mock.assert_any_call(
                    "GET",
                    expected_example_list_url,
                    params={
                        "query": '{"where": {"char": "a"}, "order": ["-char"]}',
                        "format": "csv",
                    },
                )
                request_mock.assert_called_with(
                    "PUT",
                    expected_query_report_log_detail_url,
                    data=json.dumps(expected_report_log_params),
                )


def test_query_report_task_error(
    build_client,
    mock_datetime,
    expected_query_report_options,
    expected_query_report_log_options,
    expected_query_report_detail_url,
    expected_query_report_log_detail_url,
    expected_query_report_detail_data,
    expected_query_report_log_start_data,
):
    expected_report_log_params = {
        "url": expected_query_report_log_detail_url,
        "id": 1,
        "name": "rep1",
        "report": expected_query_report_detail_url,
        "start_time": MOCKDATE.isoformat(),
        "end_time": MOCKDATE.isoformat(),
        "success": False,
    }

    responses = [
        pytest.mock_response_json(
            expected_query_report_options,
            200,
        ),
        pytest.mock_response_json(
            expected_query_report_log_options,
            200,
        ),
        pytest.mock_response_json(
            expected_query_report_detail_data,
            200,
        ),
        pytest.mock_response_json(
            expected_query_report_log_start_data,
            201,
        ),
        pytest.mock_response_json(
            expected_report_log_params,
            200,
        ),
    ]
    with mock.patch(
        "zerozero_pilot.model.authenticated_request"
    ) as request_mock:
        request_mock.side_effect = responses
        build_client.zerozero.QueryReport.metadata
        build_client.zerozero.QueryReportLog.metadata
        with mock.patch(
            "zerozero_pilot.warehouse._run_query_report_task"
        ) as task_mock:
            task_mock.side_effect = Exception("Network Error")
            with pytest.raises(Exception) as excinfo:
                run_query_report_task(1, "warehouse_url", build_client)

            # Check that stacktrace is preserved
            assert str(excinfo.value) == "Network Error"
            assert "zerozero_pilot/warehouse.py" in str(excinfo.traceback)
            assert request_mock.call_args[0] == (
                "PUT",
                expected_query_report_log_detail_url,
            )
            data = json.loads(request_mock.call_args[1]["data"])
            error = data.pop("error")
            assert data.items() == expected_report_log_params.items()
            assert "Network Error" in error


def test_get_ready_query_reports(
    build_client,
    mock_datetime,
    expected_query_report_list_url,
    expected_query_report_log_list_url,
):
    expected_report_params = {
        "query": json.dumps(
            {
                "where": {"interval__isnull": False},
                "fields": ["id", "interval"],
            },
        ),
        "format": "csv",
    }

    expected_report_log_params = {
        "query": json.dumps(
            {
                "where": {
                    "OR": [
                        {
                            "AND": [
                                {"report__interval": 240},
                                {"start_time__gt": "1999-12-31T20:00:00"},
                            ]
                        },
                        {
                            "AND": [
                                {"report__interval": 480},
                                {"start_time__gt": "1999-12-31T16:00:00"},
                            ]
                        },
                        {
                            "AND": [
                                {"report__interval": 60},
                                {"start_time__gt": "1999-12-31T23:00:00"},
                            ]
                        },
                    ]
                },
                "fields": ["report__id"],
            },
        ),
        "format": "csv",
    }

    responses = [
        pytest.mock_response_csv(
            [
                {"id": 1, "interval": 60},
                {"id": 2, "interval": 240},
                {"id": 3, "interval": 480},
                {"id": 4, "interval": 240},
            ],
            200,
        ),
        pytest.mock_response_csv([{"report__id": 2}, {"report__id": 3}], 200),
    ]

    with mock.patch(
        "zerozero_pilot.model.authenticated_request"
    ) as request_mock:
        request_mock.side_effect = responses
        report_ids = get_ready_query_reports(build_client)
        assert report_ids == [1, 4]
        request_mock.assert_any_call(
            "GET", expected_query_report_list_url, params=expected_report_params
        )
        request_mock.assert_called_with(
            "GET",
            expected_query_report_log_list_url,
            params=expected_report_log_params,
        )


def test_get_ready_query_reports_no_reports(
    build_client,
):
    response = Response()
    response.status_code = 200

    with mock.patch(
        "zerozero_pilot.model.authenticated_request"
    ) as request_mock:
        request_mock.return_value = response
        report_ids = get_ready_query_reports(build_client)
        assert report_ids == []
