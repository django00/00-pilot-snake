import csv
import io
import json
import pytest
import sys

from datetime import datetime

from requests.models import Response
from unittest import mock
from zerozero_pilot import BaseModel, exceptions
from zerozero_pilot import build_client as _build_client


def mock_response_json(content, status):
    mock_response = Response()
    mock_response.status_code = status
    mock_response._content = str.encode(json.dumps(content))
    return mock_response


pytest.mock_response_json = mock_response_json


def mock_response_csv(list_of_dicts, status):
    mock_response = Response()
    mock_response.status_code = status
    with io.StringIO() as f:
        writer = csv.DictWriter(f, list_of_dicts[0].keys())
        writer.writeheader()
        for row in list_of_dicts:
            writer.writerow(row)
        mock_response._content = f.getvalue().encode("utf-8")

    return mock_response


pytest.mock_response_csv = mock_response_csv


@pytest.fixture
def expected_root_url():
    return "http://localhost:8000/zerozero/api/"


@pytest.fixture
def expected_example_list_url(expected_root_url):
    url = "%stest_app.Example/" % (expected_root_url)
    return url


@pytest.fixture
def expected_exampleschild_list_url(expected_root_url):
    return "%stest_app.ExamplesChild/" % (expected_root_url)


@pytest.fixture
def expected_example_detail_url(expected_root_url):
    url = "%stest_app.Example/1/" % (expected_root_url)
    return url


@pytest.fixture
def expected_exampleschild_detail_url(expected_root_url):
    return "%stest_app.ExamplesChild/1/" % (expected_root_url)


@pytest.fixture
def expected_query_report_list_url(expected_root_url):
    url = "%szerozero.QueryReport/" % (expected_root_url)
    return url


@pytest.fixture
def expected_query_report_log_list_url(expected_root_url):
    url = "%szerozero.QueryReportLog/" % (expected_root_url)
    return url


@pytest.fixture
def expected_query_report_detail_url(expected_root_url):
    url = "%stest_app.QueryReport/1/" % (expected_root_url)
    return url


@pytest.fixture
def expected_query_report_log_detail_url(expected_root_url):
    url = "%stest_app.QueryReportLog/1/" % (expected_root_url)
    return url


@pytest.fixture
def expected_query_report_detail_data(expected_query_report_detail_url):
    return {
        "url": expected_query_report_detail_url,
        "id": 1,
        "name": "rep1",
        "slug": "rep1",
        "interval": "60",
        "model": "test_app.Example",
        "where": {"char": "a"},
        "fields": "",
        "order": ["-char"],
    }


@pytest.fixture
def expected_query_report_log_start_data(
    expected_query_report_detail_url, expected_query_report_log_detail_url
):
    return {
        "url": expected_query_report_log_detail_url,
        "id": 1,
        "name": "rep1",
        "report": expected_query_report_detail_url,
        "start_time": "2000-01-01T00:00:00",
        "end_time": None,
        "success": None,
        "error": None,
    }


@pytest.fixture
def expected_example_detail_data(expected_example_detail_url):
    return {
        "url": expected_example_detail_url,
        "char": "bar",
    }


@pytest.fixture
def expected_exampleschild_detail_data(
    expected_exampleschild_detail_url,
    expected_example_detail_url,
):
    # the expected response from expected_exampleschild_detail_url
    return {
        "url": expected_exampleschild_detail_url,
        "parent": expected_example_detail_url,
    }


@pytest.fixture
def expected_options(expected_example_list_url):
    return {
        "name": "test_app.Example",
        "url": expected_example_list_url,
        "description": "",
        "renders": ["application/json", "text/html", "text/csv"],
        "parses": [
            "application/json",
            "application/x-www-form-urlencoded",
            "multipart/form-data",
        ],
        "actions": {
            "POST": {
                "url": {
                    "type": "field",
                    "required": False,
                    "read_only": True,
                    "label": "Url",
                },
                "char": {
                    "type": "string",
                    "required": False,
                    "read_only": False,
                    "label": "Char",
                    "max_length": 10,
                },
            }
        },
        "fields": {
            "url": {
                "name": "url",
                "type": "relation",
                "json_type": "string",
                "required": False,
                "read_only": True,
                "related_url": expected_example_list_url,
                "related_name": "test_app.Example",
            },
            "char": {
                "name": "char",
                "type": "text",
                "json_type": "string",
                "required": True,
                "read_only": False,
            },
        },
    }


@pytest.fixture
def expected_options_read_only(expected_example_list_url):
    return {
        "name": "test_app.Example",
        "description": "",
        "renders": ["application/json", "text/html", "text/csv"],
        "parses": [
            "application/json",
            "application/x-www-form-urlencoded",
            "multipart/form-data",
        ],
        "actions": {
            "POST": {
                "url": {
                    "type": "field",
                    "required": False,
                    "read_only": True,
                    "label": "Url",
                },
                "char": {
                    "type": "string",
                    "required": False,
                    "read_only": True,
                    "label": "Char",
                    "max_length": 10,
                },
            }
        },
        "fields": {
            "url": {
                "name": "url",
                "type": "relation",
                "json_type": "string",
                "required": False,
                "read_only": True,
                "related_url": expected_example_list_url,
                "related_name": "test_app.Example",
            },
            "char": {
                "name": "char",
                "type": "text",
                "json_type": "string",
                "required": True,
                "read_only": True,
            },
        },
    }


@pytest.fixture
def Example(
    set_token,
    expected_example_list_url,
    expected_options,
):
    class _Example(BaseModel):
        list_url = expected_example_list_url

    _Example._metadata = expected_options
    return _Example


@pytest.fixture
def expected_options_with_fk(
    expected_exampleschild_list_url, expected_example_list_url, Example
):
    return {
        "name": "test_app.ExamplesChild",
        "url": expected_exampleschild_list_url,
        "description": "",
        "renders": ["application/json", "text/html", "text/csv"],
        "parses": [
            "application/json",
            "application/x-www-form-urlencoded",
            "multipart/form-data",
        ],
        "actions": {
            "POST": {
                "url": {
                    "type": "field",
                    "required": False,
                    "read_only": True,
                    "label": "Url",
                },
                "parent": {
                    "type": "field",
                    "required": True,
                    "read_only": False,
                    "label": "Parent",
                },
            }
        },
        "fields": {
            "url": {
                "name": "url",
                "type": "relation",
                "json_type": "string",
                "required": False,
                "read_only": True,
                "related_url": expected_exampleschild_list_url,
                "related_name": "test_app.ExamplesChild",
            },
            "parent": {
                "name": "parent",
                "type": "relation",
                "json_type": "string",
                "required": True,
                "read_only": False,
                "related_url": expected_example_list_url,
                "related_model": Example,
                "related_name": "test_app.Example",
            },
        },
    }


@pytest.fixture
def build_client(
    expected_example_list_url,
    expected_exampleschild_list_url,
    expected_query_report_list_url,
    expected_query_report_log_list_url,
    expected_root_url,
):
    TOKEN = "test-token"

    mock_response = mock_response_json(
        {
            "test_app.Example": expected_example_list_url,
            "test_app.ExamplesChild": expected_exampleschild_list_url,
            "zerozero.QueryReport": expected_query_report_list_url,
            "zerozero.QueryReportLog": expected_query_report_log_list_url,
        },
        200,
    )

    with mock.patch("zerozero_pilot.request.authenticated_request") as ar:
        ar.return_value = mock_response
        _build_client(expected_root_url, TOKEN, __name__)
    from zerozero_pilot import model  # have to do after build client

    return model.models


@pytest.fixture
def mock_response_options(expected_options):
    return mock_response_json(
        expected_options,
        200,
    )


@pytest.fixture
def mock_response_options_read_only(expected_options_read_only):
    return mock_response_json(
        expected_options_read_only,
        200,
    )


@pytest.fixture
def mock_response_options_with_fk(
    expected_example_list_url, expected_exampleschild_list_url
):
    options = {
        "name": "test_app.ExamplesChild",
        "url": expected_exampleschild_list_url,
        "description": "",
        "renders": ["application/json", "text/html", "text/csv"],
        "parses": [
            "application/json",
            "application/x-www-form-urlencoded",
            "multipart/form-data",
        ],
        "actions": {
            "POST": {
                "url": {
                    "type": "field",
                    "required": False,
                    "read_only": True,
                    "label": "Url",
                },
                "parent": {
                    "type": "field",
                    "required": True,
                    "read_only": False,
                    "label": "Parent",
                },
            }
        },
        "fields": {
            "url": {
                "name": "url",
                "type": "relation",
                "json_type": "string",
                "required": False,
                "read_only": True,
                "related_url": expected_exampleschild_list_url,
                "related_name": "test_app.ExamplesChild",
            },
            "parent": {
                "name": "parent",
                "type": "relation",
                "json_type": "string",
                "required": True,
                "read_only": False,
                "related_url": expected_example_list_url,
                "related_name": "test_app.Example",
            },
        },
    }

    return mock_response_json(
        options,
        200,
    )


@pytest.fixture
def mock_response_example_detail(expected_example_detail_url):
    return mock_response_json(
        {
            "url": expected_example_detail_url,
            "char": "bar",
        },
        200,
    )


@pytest.fixture
def set_token():
    client_module = sys.modules["zerozero_pilot.request"]
    client_module.TOKEN = "test-token"
    yield client_module.TOKEN
    client_module.TOKEN = None


@pytest.fixture
def expected_query_report_options(expected_query_report_list_url):
    return {
        "name": "zerozero.QueryReport",
        "description": "",
        "renders": ["application/json", "text/html", "text/csv"],
        "parses": [
            "application/json",
            "application/x-www-form-urlencoded",
            "multipart/form-data",
        ],
        "actions": {
            "POST": {
                "url": {
                    "type": "field",
                    "required": False,
                    "read_only": True,
                    "label": "Url",
                },
                "name": {
                    "type": "string",
                    "required": True,
                    "read_only": False,
                    "label": "Name",
                    "max_length": 255,
                },
                "slug": {
                    "type": "slug",
                    "required": True,
                    "read_only": False,
                    "label": "Slug",
                    "max_length": 50,
                },
                "model": {
                    "type": "string",
                    "required": True,
                    "read_only": False,
                    "label": "Model",
                    "max_length": 255,
                },
                "fields": {
                    "type": "string",
                    "required": False,
                    "read_only": False,
                    "label": "Fields",
                },
                "order": {
                    "type": "string",
                    "required": False,
                    "read_only": False,
                    "label": "Order",
                },
                "where": {
                    "type": "string",
                    "required": False,
                    "read_only": False,
                    "label": "Where",
                },
                "interval": {
                    "type": "integer",
                    "required": False,
                    "read_only": False,
                    "label": "Interval",
                    "min_value": -2147483648,
                    "max_value": 2147483647,
                },
                "start_time": {
                    "type": "datetime",
                    "required": False,
                    "read_only": False,
                    "label": "Start time",
                },
            }
        },
        "fields": {
            "url": {
                "name": "url",
                "type": "relation",
                "json_type": "string",
                "required": False,
                "read_only": True,
                "related_url": expected_query_report_list_url,
                "related_name": "zerozero.QueryReport",
            },
            "name": {
                "name": "name",
                "type": "text",
                "json_type": "string",
                "required": True,
                "read_only": False,
            },
            "slug": {
                "name": "slug",
                "type": "slug",
                "json_type": "string",
                "required": True,
                "read_only": False,
            },
            "model": {
                "name": "model",
                "type": "text",
                "json_type": "string",
                "required": True,
                "read_only": False,
            },
            "fields": {
                "name": "fields",
                "type": "json",
                "json_type": "object",
                "required": False,
                "read_only": False,
            },
            "order": {
                "name": "order",
                "type": "json",
                "json_type": "object",
                "required": False,
                "read_only": False,
            },
            "where": {
                "name": "where",
                "type": "json",
                "json_type": "object",
                "required": False,
                "read_only": False,
            },
            "interval": {
                "name": "interval",
                "type": "number",
                "json_type": "number",
                "required": False,
                "read_only": False,
            },
            "start_time": {
                "name": "start_time",
                "type": "datetime",
                "json_type": "string",
                "required": False,
                "read_only": False,
            },
        },
        "url": expected_query_report_list_url,
    }


@pytest.fixture
def expected_query_report_log_options(
    expected_query_report_list_url, expected_query_report_log_list_url
):
    return {
        "name": "zerozero.QueryReportLog",
        "description": "",
        "renders": ["application/json", "text/html", "text/csv"],
        "parses": [
            "application/json",
            "application/x-www-form-urlencoded",
            "multipart/form-data",
        ],
        "actions": {
            "POST": {
                "url": {
                    "type": "field",
                    "required": False,
                    "read_only": True,
                    "label": "Url",
                },
                "name": {
                    "type": "string",
                    "required": True,
                    "read_only": False,
                    "label": "Name",
                    "max_length": 255,
                },
                "start_time": {
                    "type": "datetime",
                    "required": True,
                    "read_only": False,
                    "label": "Start time",
                },
                "end_time": {
                    "type": "datetime",
                    "required": False,
                    "read_only": False,
                    "label": "End time",
                },
                "success": {
                    "type": "boolean",
                    "required": False,
                    "read_only": False,
                    "label": "Success",
                },
                "error": {
                    "type": "string",
                    "required": False,
                    "read_only": False,
                    "label": "Error",
                },
                "report": {
                    "type": "field",
                    "required": False,
                    "read_only": False,
                    "label": "Report",
                },
            }
        },
        "fields": {
            "url": {
                "name": "url",
                "type": "relation",
                "json_type": "string",
                "required": False,
                "read_only": True,
                "related_url": expected_query_report_log_list_url,
                "related_name": "zerozero.QueryReportLog",
            },
            "name": {
                "name": "name",
                "type": "text",
                "json_type": "string",
                "required": True,
                "read_only": False,
            },
            "start_time": {
                "name": "start_time",
                "type": "datetime",
                "json_type": "string",
                "required": True,
                "read_only": False,
            },
            "end_time": {
                "name": "end_time",
                "type": "datetime",
                "json_type": "string",
                "required": False,
                "read_only": False,
            },
            "success": {
                "name": "success",
                "type": "checkbox",
                "json_type": "boolean",
                "required": False,
                "read_only": False,
            },
            "error": {
                "name": "error",
                "type": "text",
                "json_type": "string",
                "required": False,
                "read_only": False,
            },
            "report": {
                "name": "report",
                "type": "relation",
                "json_type": "string",
                "required": False,
                "read_only": False,
                "related_url": expected_query_report_list_url,
                "related_name": "zerozero.QueryReport",
            },
        },
        "url": expected_query_report_log_list_url,
    }
