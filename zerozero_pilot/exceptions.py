class ZeroZeroClientException(Exception):
    pass


class ZeroZeroAPIException(Exception):
    pass


class ZeroZeroValidationException(Exception):
    pass


class ZeroZeroReadOnlyException(Exception):
    pass
