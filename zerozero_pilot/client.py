import sys

from requests import request
from zerozero_pilot import exceptions, BaseModel
from zerozero_pilot import model
from zerozero_pilot import request


class Root:
    pass


class App:
    pass


def build_client(api_root_url: str, token: str, module_name: str):
    request.TOKEN = token
    model.models = Root()
    module = sys.modules[module_name]
    response = request.authenticated_request("GET", api_root_url)
    if response.status_code != 200:
        raise exceptions.ZeroZeroAPIException(response.content)
    models = response.json()

    class App:
        pass

    for path, url_value in models.items():
        app_name, model_name = path.split(".")
        app = getattr(model.models, app_name, None)
        if not app:
            app = App()
            setattr(model.models, app_name, app)
            setattr(module, app_name, app)

        class CurrentBaseModel(BaseModel):
            list_url = url_value

        CurrentBaseModel.__name__ = path
        app.__dict__[model_name] = CurrentBaseModel

    module.models = model.models


def get_model(path: str, models):
    app_name, model_name = path.split(".")
    app = get_app(app_name, models)
    Model = getattr(app, model_name)
    return Model


def get_app(app_name: str, models):
    app = getattr(models, app_name)
    return app
